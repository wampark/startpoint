export default {
    xlv: {
      id:151 ,
      nome: "XLV",
      subnome: null,
      imgs: {
        bg: require('./../../assets/img/xw.svg'),
        frente: require('./../../assets/img/xlv/detalhes/frente.png'),
        frente1: require('./../../assets/img/xlv/detalhes/frente1.png'),
        frente2: require('./../../assets/img/xlv/detalhes/frente2.png'),
        frente3: require('./../../assets/img/xlv/detalhes/frente3.png'),
        lateral: require('./../../assets/img/xlv/detalhes/lateral.png'),
        lateral1: require('./../../assets/img/xlv/detalhes/lateral1.png'),
        lateral2: require('./../../assets/img/xlv/detalhes/lateral2.png'),
        lateral3: require('./../../assets/img/xlv/detalhes/lateral3.png'),
        traseira: require('./../../assets/img/xlv/detalhes/traseira.png'),
        traseira1: require('./../../assets/img/xlv/detalhes/traseira1.png'),
        traseira2: require('./../../assets/img/xlv/detalhes/traseira2.png'),
        traseira3: require('./../../assets/img/xlv/detalhes/traseira3.png'),
        motor: require('./../../assets/img/motor-tivoli.png'),
        fichaTecnica: require('./../../assets/img/xlv/ficha_tecnica.png'),
      },
      valorSTD: "97.900",
      valorDLX: "109.900",
      textos: {
        motor1: "MOTOR EURO 5",
        motor2: "A GASOLINA DE 1.6L",
        motor3: "De 128 cv e 16 kgfm de torque e câmbio automático de seis velocidades."
      },
      conheca: [
        {
          nome: "ACTYON SPORTS",
          route: "actyon",
          img: require('./../../assets/img/a.png'),
        },
        {
          nome: "KORANDO",
          route: "korando",
          img: require('./../../assets/img/k.png'),
        },
        {
          nome: "TIVOLI",
          route: "tivoli",
          img: require('./../../assets/img/t.png'),
        },
      ]
    },
    tivoli: {
      id:128 ,
      nome: "Tivoli",
      subnome: null,
      imgs: {
        bg: require('./../../assets/img/xw.svg'),
        frente: require('./../../assets/img/tivoli/detalhes/frente.png'),
        frente1: require('./../../assets/img/tivoli/detalhes/frente1.png'),
        frente2: require('./../../assets/img/tivoli/detalhes/frente2.png'),
        frente3: require('./../../assets/img/tivoli/detalhes/frente3.png'),
        lateral: require('./../../assets/img/tivoli/detalhes/lateral.png'),
        lateral1: require('./../../assets/img/tivoli/detalhes/lateral1.png'),
        lateral2: require('./../../assets/img/tivoli/detalhes/lateral2.png'),
        lateral3: require('./../../assets/img/tivoli/detalhes/lateral3.png'),
        traseira: require('./../../assets/img/tivoli/detalhes/traseira.png'),
        traseira1: require('./../../assets/img/tivoli/detalhes/traseira1.png'),
        traseira2: require('./../../assets/img/tivoli/detalhes/traseira2.png'),
        traseira3: require('./../../assets/img/tivoli/detalhes/traseira3.png'),
        motor: require('./../../assets/img/motor-tivoli.png'),
        fichaTecnica: require('./../../assets/img/tivoli/ficha_tecnica.png'),
      },
      valorSTD: "84.900",
      valorDLX: "99.900",
      textos: {
        motor1: "MOTOR EURO 5",
        motor2: "A GASOLINA DE 1.6L",
        motor3: "De 128 cv e 16 kgfm de torque e câmbio automático de seis velocidades."
      },
      conheca: [
        {
          nome: "XLV",
          route: "xlv",
          img: require('./../../assets/img/x.png'),
        },
        {
          nome: "ACTYON SPORTS",
          route: "actyon",
          img: require('./../../assets/img/a.png'),
        },
        {
          nome: "KORANDO",
          route: "korando",
          img: require('./../../assets/img/k.png'),
        },
      ]
    },
    korando: {
      id:346 ,
      nome: "Korando",
      subnome: null,
      imgs: {
        bg: require('./../../assets/img/xw.svg'),
        frente: require('./../../assets/img/xlv/detalhes/frente.png'),
        frente1: require('./../../assets/img/xlv/detalhes/frente.png'),
        frente2: require('./../../assets/img/xlv/detalhes/frente.png'),
        frente3: require('./../../assets/img/xlv/detalhes/frente.png'),
        lateral: require('./../../assets/img/xlv/detalhes/lateral.png'),
        lateral1: require('./../../assets/img/xlv/detalhes/lateral.png'),
        lateral2: require('./../../assets/img/xlv/detalhes/lateral.png'),
        lateral3: require('./../../assets/img/xlv/detalhes/lateral.png'),
        traseira: require('./../../assets/img/xlv/detalhes/traseira.png'),
        traseira1: require('./../../assets/img/xlv/detalhes/traseira.png'),
        traseira2: require('./../../assets/img/xlv/detalhes/traseira.png'),
        traseira3: require('./../../assets/img/xlv/detalhes/traseira.png'),
        motor: require('./../../assets/img/actyon/motor.png'),
        fichaTecnica: require('./../../assets/img/korando/ficha_tecnica.png'),
      },
      valorSTD: "129.900",
      valorDLX: "149.900",
      textos: {
        motor1: "MOTOR EURO 5",
        motor2: "A DIESEL 2.2 TURBO",
        motor3: "De 178 cv e 41 kgfm de torque e câmbio automático de seis velocidades AISIN."
      },
      conheca: [
        {
          nome: "XLV",
          route: "xlv",
          img: require('./../../assets/img/x.png'),
        },
        {
          nome: "ACTYON SPORTS",
          route: "actyon",
          img: require('./../../assets/img/a.png'),
        },
        {
          nome: "TIVOLI",
          route: "tivoli",
          img: require('./../../assets/img/t.png'),
        },
      ]
    },
    actyon: {
      id:286 ,
      nome: "Actyon",
      subnome: "Sports",
      imgs: {
        bg: require('./../../assets/img/xw.svg'),
        frente: require('./../../assets/img/xlv/detalhes/frente.png'),
        frente1: require('./../../assets/img/xlv/detalhes/frente1.png'),
        frente2: require('./../../assets/img/xlv/detalhes/frente2.png'),
        frente3: require('./../../assets/img/xlv/detalhes/frente3.png'),
        lateral: require('./../../assets/img/xlv/detalhes/lateral.png'),
        lateral1: require('./../../assets/img/xlv/detalhes/lateral1.png'),
        lateral2: require('./../../assets/img/xlv/detalhes/lateral2.png'),
        lateral3: require('./../../assets/img/xlv/detalhes/lateral3.png'),
        traseira: require('./../../assets/img/xlv/detalhes/traseira.png'),
        traseira1: require('./../../assets/img/xlv/detalhes/traseira1.png'),
        traseira2: require('./../../assets/img/xlv/detalhes/traseira2.png'),
        traseira3: require('./../../assets/img/xlv/detalhes/traseira3.png'),
        motor: require('./../../assets/img/actyon/motor.png'),
        fichaTecnica: require('./../../assets/img/actyon/ficha_tecnica.png'),
      },
      valorSTD: "129.900",
      valorDLX: "149.900",
      textos: {
        motor1: "MOTOR EURO 5",
        motor2: "A DIESEL 2.2 TURBO 4x4",
        motor3: "De 178 cv e 41 kgfm de torque e câmbio automático de seis velocidades AISIN."
      },
      conheca: [
        {
          nome: "XLV",
          route: "xlv",
          img: require('./../../assets/img/x.png'),
        },
        {
          nome: "KORANDO",
          route: "korando",
          img: require('./../../assets/img/k.png'),
        },
        {
          nome: "TIVOLI",
          route: "tivoli",
          img: require('./../../assets/img/t.png'),
        },
      ]
    }
  };