export default function (numero) {
  if (numero!==undefined && numero!==null) {
    var formated = parseInt(numero).toFixed(2).split('.');
    formated[0] = "R$ " + formated[0].split(/(?=(?:...)*$)/).join('.');
    return formated.join(",");
  }
  return numero;
}


