import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueResource from 'vue-resource';
import Vue2Filters from 'vue2-filters';
import VueMask from 'di-vue-mask'
import App from './App';
import router from './router';
import $store from 'store';
import store from './store/store';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/pt-br'
import './assets/scss/_global.scss';
Vue.use(ElementUI, { locale });

Vue.config.productionTip = false;
/* Event vuejs modo de usar:  this.$event.$emit() || this.$event.$on() */
Vue.prototype.$event = new Vue();
Vue.use(Vuelidate);
Vue.use(VueResource);
Vue.use(Vue2Filters);
/** Activate vue.js plugins **/
Vue.use(VueMask);

//seta a url da api em  congfig/dev.env.js/API_URL
Vue.http.options.root = `${process.env.API_URL}`;

//Interceptos
Vue.http.interceptors.push((request, next) => {
  let tokenAuth,
      obj;

  //verifica se existe token_auth em $store
  $store.get('token_auth') !==  null && $store.get('token_auth') !== undefined
     ?(
       obj = $store.get('token_auth'),
       tokenAuth = obj
      )
     : tokenAuth = null;
  // caso let tokenAuth retorne true envia o token
  if(tokenAuth) {
    request.headers.set('Authorization', `Bearer ${tokenAuth}`);
    request.headers.set("Access-Control-Allow-Origin", "*");
    request.headers.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  }
  next();
});

Vue.mixin({
  data() {
    return {
      screen: 0
    };
  },
  computed: {
  },
  methods: {
  },
  mounted() {
    /**
     * @var $width recupera o width atual do windows
     * @return o valor do width adicionado no state da aplicação...
     * recupera o valor com this.$store.state.width
     */
    let $width = window.innerWidth;
    window.onresize = () => {
      $width = window.innerWidth;
      store.commit('SET_WIDTH', $width);
      this.screen = $width;
    };
    store.commit('SET_WIDTH', $width);
    this.screen = $width;
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
