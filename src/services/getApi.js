import Vue from 'vue';

class getApi {

    get() {
        return Vue.http.get('posts');
    }

}

export default new getApi();