import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/layouts/default';
import Home from '@/pages/Home';
import Dash from '@/pages/dash';
import store from 'store';

Vue.use(Router);

const route = new Router({
  routes: [
    {
      path: '*',
      name: '404',
      redirect: '/',
    },
    {
      path: '',
      name: 'default',
      component: Layout,
      beforeEnter: (to, from, next) => {
        next();
      },
      children: [
        {
          path: '/',
          name: 'home',
          component: Home,
        },
        {
          path: '/dash',
          name: 'dash',
          component: Dash,
          meta:{requiresAuth: true}
        },
      ],
    },
  ],
  mode: 'history',
  linkActiveClass: 'active',
});

route.beforeEach((to, from, next) => {

  let  obj, tokenAuth;

  store.get('user') !== undefined && store.get('user') !== null ?
  (
    obj = store.get('user'),
    tokenAuth = obj
  ) : false

  let  require = to.matched.some(record => record.meta.requiresAuth),
       requireNot = to.matched.some(record => record.meta.requireNot);

  if(require && !tokenAuth) next({name:'home'})
  else if(requireNot && tokenAuth) next({name:'dash'})

  else next()
})

export default route;
