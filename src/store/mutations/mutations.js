export default {
  SET_URL(state, url) {
    state.url = url;
  },
  SET_LOGIN_ACTION(state, action) {
    state.action = action;
  },
  ACTIVE_ACTIVE(state) {
    state.active = true;
  },
  SET_WIDTH(state, value) {
    state.width = value;
  },
  FULL(state) {
    state.full = true;
  }
};
