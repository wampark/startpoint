import login from './login/stateLogin';

export default {
  url: null,
  active: false,
  width: '',
  full: false,
  login,
};
